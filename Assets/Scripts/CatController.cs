﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CatController : MonoBehaviour {
    [SerializeField] private GameObject patrolPoints;
    private List<Transform> points;
    private int destinationPointIndex;
    private NavMeshAgent agent;

    private void Start() {
        points = new List<Transform>();
        foreach (var point in patrolPoints.transform.GetComponentsInChildren<Transform>()) {
            if (point.gameObject.GetInstanceID() != patrolPoints.GetInstanceID()) {
                points.Add(point);
            }
        }

        destinationPointIndex = 0;
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        GotoNextPoint();
    }

    private void Update() {
        if (!agent.pathPending && agent.remainingDistance < 0.5f) {
            GotoNextPoint();
        }
    }

    public void GotoNextPoint() {
        if (points.Count > 0) {
            agent.destination = points[destinationPointIndex].position;
            destinationPointIndex = (destinationPointIndex + 1) % points.Count;
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("CatDistractor")) {
            agent.destination = other.transform.position;
        }
    }
}