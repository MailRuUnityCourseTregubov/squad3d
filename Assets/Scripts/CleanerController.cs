﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class CleanerController : MonoBehaviour {
    private enum Task {
        Clean,
        Help,
        Distract,
        None
    }

    private Camera mainCamera;
    private CleanerSelector cleanerSelector;
    private Transform player;
    private NavMeshAgent agent;

    private bool isEnable;
    private bool isControlledByPlayer;
    private Task currentTask;

    private Queue<Vector3> path;
    private float defaultStoppingDistance;


    public bool IsEnable {
        get => isEnable;
        set {
            isEnable = value;
            gameObject.GetComponent<NavMeshAgent>().enabled = value;
            gameObject.GetComponent<NavMeshObstacle>().enabled = !value;
            gameObject.tag = value ? "Player" : "BrokenPlayer";
            if (!value) {
                CurrentTask = Task.None;
                cleanerSelector.SelectNextNoneTask();
            }
        }
    }

    public bool IsControlledByPlayer {
        get => isControlledByPlayer;
        set {
            isControlledByPlayer = value;
            player = cleanerSelector.SelectedCleaner().transform;
            if (!value && CurrentTask == Task.None) {
                agent.stoppingDistance = defaultStoppingDistance;
            }
        }
    }

    private Task CurrentTask {
        get => currentTask;
        set {
            if (CurrentTask == Task.Distract && value != Task.Distract) {
                gameObject.transform.Find("CatDistractor").gameObject.SetActive(false);
            }

            currentTask = value;
            if (value != Task.None) {
                cleanerSelector.SelectNextNoneTask();
                agent.stoppingDistance = 0;
            }
            else {
                agent.stoppingDistance = IsControlledByPlayer ? 0 : defaultStoppingDistance;
            }
        }
    }


    private void Awake() {
        if (Camera.main != null) mainCamera = Camera.main;
        cleanerSelector = GameObject.FindGameObjectWithTag("GameController").GetComponent<CleanerSelector>();
        agent = GetComponent<NavMeshAgent>();
        defaultStoppingDistance = agent.stoppingDistance;
    }

    private void Start() {
        player = cleanerSelector.SelectedCleaner().transform;
        IsEnable = true;
        CurrentTask = Task.None;
    }

    private void Update() {
        if (IsEnable) {
            DoTask(CurrentTask);
        }
    }


    public void MoveToClick() {
        if (IsEnable && IsControlledByPlayer && !EventSystem.current.IsPointerOverGameObject()) {
            CurrentTask = Task.None;
            if ( Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit)) {
                MoveTo(hit.point);
            }
        }
    }

    public bool IsAvailable() {
        return IsEnable && CurrentTask == Task.None;
    }

    private void MoveTo(Vector3 position) {
        agent.destination = position;
    }


    private void DoTask(Task task) {
        switch (task) {
            case Task.Clean:
                Clean();
                break;
            case Task.Help:
                Help();
                break;
            case Task.Distract:
                Distract();
                break;
            case Task.None:
                if (!IsControlledByPlayer) {
                    MoveTo(player.position);
                }

                break;
        }
    }


    public void GiveCleanTask(Queue<Vector3> cleanPath) {
        if (IsEnable) {
            CurrentTask = Task.Clean;
            path = cleanPath;
        }
    }

    public void GiveHelpTask() {
        if (IsEnable) {
            if (GameObject.FindGameObjectsWithTag("BrokenPlayer").Length > 0) {
                CurrentTask = Task.Help;
                path = new Queue<Vector3>();
                path.Enqueue(GameObject.FindGameObjectWithTag("BrokenPlayer").transform.position);
            }
        }
    }

    public void GiveDistractTask(Queue<Vector3> distractPath) {
        if (IsEnable) {
            CurrentTask = Task.Distract;
            path = distractPath;
            gameObject.transform.Find("CatDistractor").gameObject.SetActive(true);
        }
    }


    private void Clean() {
        if (path.Count > 0) {
            FollowPath();
        }
        else {
            CurrentTask = Task.None;
        }
    }

    private void Help() {
        if (path.Count > 0) {
            FollowPath();
        }
        else {
            CurrentTask = Task.None;
        }
    }

    private void Distract() {
        if (path.Count > 0) {
            FollowPath();
        }
    }


    private void FollowPath() {
        agent.destination = path.Peek();
        if (!agent.pathPending && agent.remainingDistance < 0.5f) {
            path.Dequeue();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("BrokenPlayer")) {
            other.GetComponent<CleanerController>().IsEnable = true;
        }
    }
}