﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CleanerSelector : MonoBehaviour {
    [SerializeField] private GameObject toggleLayout;
    [SerializeField] private Toggle togglePrefab;
    private List<Toggle> cleanerToggles;

    private List<CleanerController> cleaners;
    private CameraController cameraController;
    private int selected;

    private void Awake() {
        cleaners = new List<CleanerController>();
        foreach (var cleaner in gameObject.GetComponent<GameManager>().Cleaners) {
            cleaners.Add(cleaner.GetComponent<CleanerController>());
        }

        cleanerToggles = new List<Toggle>();
        for (int i = 0; i < cleaners.Count; i++) {
            var cleanerToggle = Instantiate(togglePrefab, toggleLayout.transform);
            cleanerToggle.onValueChanged.AddListener(delegate { ToggleValueChanged(cleanerToggle); });
            cleanerToggles.Add(cleanerToggle);
        }

        if (Camera.main != null) cameraController = Camera.main.GetComponent<CameraController>();
        Select(0);
    }

    private void ToggleValueChanged(Toggle change) {
        Select(cleanerToggles.IndexOf(change));
    }

    public CleanerController SelectedCleaner() {
        return cleaners[selected];
    }

    public void SelectNext() {
        Select(selected < cleaners.Count - 1 ? selected + 1 : 0);
    }

    public void SelectNextNoneTask() {
        if (cleaners.All(o => !o.IsEnable)) {
            gameObject.GetComponent<GameManager>().GameOver(false);
        }

        for (int i = selected < cleaners.Count - 1 ? selected + 1 : 0;
            i != selected;
            i = i < cleaners.Count - 1 ? i + 1 : 0) {
            if (cleaners[i].IsAvailable()) {
                Select(i);
                break;
            }
        }
    }

    private void Select(int index) {
        selected = index;
        cameraController.Player = cleaners[selected].gameObject;
        foreach (var cleaner in cleaners) {
            cleaner.IsControlledByPlayer = cleaners.IndexOf(cleaner) == index;
        }

        // it is a workaround; unity 2019 have SetIsOnWithoutNotify() method
        foreach (var cleanerToggle in cleanerToggles) {
            cleanerToggle.onValueChanged.RemoveAllListeners();
            cleanerToggle.isOn = cleanerToggles.IndexOf(cleanerToggle) == index;
            cleanerToggle.onValueChanged.AddListener(delegate { ToggleValueChanged(cleanerToggle); });
        }
    }
}