﻿using System.Collections.Generic;
using UnityEngine;

public class CommandsDisplay : MonoBehaviour {
    [SerializeField] private GameObject cleanAreaDisplayPrefab;
    [SerializeField] private GameObject distractLineDisplayPrefab;
    [SerializeField] private float cleanerStep;

    private GameObject cleanAreaDisplay;
    private GameObject distractLineDisplay;

    private LineRenderer distractLineRenderer;
    private bool distractLineFirstClick;

    private Camera mainCamera;


    public bool CleanAreaIsActive => cleanAreaDisplay.activeSelf;
    public bool DistractLineIsActive => distractLineDisplay.activeSelf;

    public bool DistractLineFirstClick {
        get => distractLineFirstClick;
        set => distractLineFirstClick = value;
    }


    public void ShowCleanArea(bool show) {
        cleanAreaDisplay.SetActive(show);
        if (show && DistractLineIsActive) {
            ShowDistractLine(false);
        }
    }

    public void ShowDistractLine(bool show) {
        if (show) {
            MoveDistractLineFirstPointToMouse();
        }

        distractLineDisplay.SetActive(show);
        distractLineFirstClick = show;
        if (show && CleanAreaIsActive) {
            ShowCleanArea(false);
        }
    }

    public void HideCommandsVisualisation() {
        ShowCleanArea(false);
        ShowDistractLine(false);
    }


    public Queue<Vector3> ReleaseCleanArea() {
        ShowCleanArea(false);

        var queue = new Queue<Vector3>();

        var k = (int) (cleanAreaDisplay.GetComponent<MeshFilter>().mesh.bounds.size.x / cleanerStep);
        for (int i = 0; i < k * 2; i++) {
            queue.Enqueue(new Vector3(k / 2f * cleanerStep - i / 2f * cleanerStep, 0,
                              -k / 2f * cleanerStep + ((i + 1) / 2 % 2) * k * cleanerStep) +
                          cleanAreaDisplay.transform.position);
        }

        return queue;
    }

    public Queue<Vector3> ReleaseDistractLine() {
        ShowDistractLine(false);

        var queue = new Queue<Vector3>();

        queue.Enqueue(distractLineRenderer.GetPosition(0));
        queue.Enqueue(distractLineRenderer.GetPosition(1));

        return queue;
    }


    private void Start() {
        cleanAreaDisplay = Instantiate(cleanAreaDisplayPrefab);
        cleanAreaDisplay.SetActive(false);

        distractLineDisplay = Instantiate(distractLineDisplayPrefab);
        distractLineDisplay.SetActive(false);

        distractLineRenderer = distractLineDisplay.GetComponent<LineRenderer>();

        if (Camera.main != null) mainCamera = Camera.main;
    }

    private void Update() {
        if (CleanAreaIsActive) {
            MoveCleanAreaToMouse();
        }

        if (DistractLineIsActive) {
            if (DistractLineFirstClick) {
                MoveDistractLineFirstPointToMouse();
            }
            else {
                MoveDistractLineEndPointToMouse();
            }
        }
    }


    private void MoveCleanAreaToMouse() {
        if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit,
            100, LayerMask.GetMask("Ground"))) {
            cleanAreaDisplay.transform.position = hit.point + new Vector3(0, 0.1f, 0);
            cleanAreaDisplay.transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
        }
    }

    private void MoveDistractLineFirstPointToMouse() {
        if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit,
            100, LayerMask.GetMask("Ground"))) {
            distractLineRenderer.SetPosition(0, hit.point);
            distractLineRenderer.SetPosition(1, hit.point);
        }
    }

    private void MoveDistractLineEndPointToMouse() {
        if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit,
            100, LayerMask.GetMask("Ground"))) {
            distractLineRenderer.SetPosition(1, hit.point);
        }
    }
}