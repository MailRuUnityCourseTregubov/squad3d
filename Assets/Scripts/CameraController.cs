﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    [SerializeField] private int movementSpeed;
    private GameObject player;
    private Vector3 offset;

    public GameObject Player {
        get => player;
        set => player = value;
    }

    private void Start() {
        offset = transform.position - Player.transform.position;
    }

    private void LateUpdate() {
        transform.position = Vector3.Slerp(transform.position, Player.transform.position + offset,
            Time.deltaTime * movementSpeed);
    }
}