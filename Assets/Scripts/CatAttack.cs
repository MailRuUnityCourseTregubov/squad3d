﻿using UnityEngine;

public class CatAttack : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            other.GetComponent<CleanerController>().IsEnable = false;
            gameObject.GetComponentInParent<CatController>().GotoNextPoint();
        }
    }
}