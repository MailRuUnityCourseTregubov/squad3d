﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CleanFloor : MonoBehaviour {
    [SerializeField] private float requiredCleanedPercent;
    [SerializeField] private Text percentLabel;

    private GameManager gameManager;
    private List<GameObject> cleaners;
    private int cleanerTrail;

    private bool[,] isCleanedArray;
    private float cleanedPercent;
    private float cleanedPercentIncrement;

    private Texture2D texture;
    private Color[] colors;

    private void Start() {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        cleaners = gameManager.Cleaners;

        var mainTexture = gameObject.GetComponent<Renderer>().material.mainTexture;
        texture = new Texture2D(mainTexture.width, mainTexture.height);
        Graphics.CopyTexture(mainTexture, texture);
        gameObject.GetComponent<Renderer>().material.mainTexture = texture;

        // max square inside circle
        cleanerTrail = Mathf.RoundToInt(cleaners[0].GetComponentInChildren<MeshFilter>().mesh.bounds.size.x *
                                        Mathf.Sqrt(2) * mainTexture.width * 0.01f);

        colors = new Color[cleanerTrail * cleanerTrail];
        for (int i = 0; i < colors.Length; i++) {
            colors[i] = Color.clear;
        }

        // one cell equals one square unit
        var boundsSize = gameObject.GetComponent<MeshRenderer>().bounds.size;
        isCleanedArray = new bool[(int) boundsSize.x, (int) boundsSize.z];
        cleanedPercentIncrement = 100f / isCleanedArray.Length;
        cleanedPercent = 0;
    }

    private float CleanedPercent {
        get => cleanedPercent;
        set {
            cleanedPercent = value; 
            percentLabel.text = Mathf.RoundToInt(cleanedPercent) + "%";
        }
    }

    private void Update() {
        foreach (var cleaner in cleaners) {
            if (Physics.Raycast(cleaner.transform.position, Vector3.down, out var hit)) {
                texture.SetPixels(
                    (int) (hit.textureCoord.x * texture.width - cleanerTrail / 2f),
                    (int) (hit.textureCoord.y * texture.height - cleanerTrail / 2f),
                    cleanerTrail, cleanerTrail, colors);

                // the floor occupies the bottom half of the UV
                var cellInIsCleanedArray = new Vector2(hit.textureCoord.x * isCleanedArray.GetLength(0),
                    hit.textureCoord.y * isCleanedArray.GetLength(1) * 2);

                if (!isCleanedArray[(int) cellInIsCleanedArray.x, (int) cellInIsCleanedArray.y]) {
                    isCleanedArray[(int) cellInIsCleanedArray.x, (int) cellInIsCleanedArray.y] = true;
                    CleanedPercent += cleanedPercentIncrement;
//                    Debug.Log(cellInIsCleanedArray);
                }
            }
        }

        if (CleanedPercent >= requiredCleanedPercent) {
            gameManager.GameOver(true);
        }

        texture.Apply();
    }
}