﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    [SerializeField] private List<GameObject> cleaners;

    [SerializeField] private Button startButton;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button nextLevelButton;

    private CleanerSelector cleanerSelector;
    private CommandsDisplay commandsDisplay;

    private bool isInputEnabled;

    public List<GameObject> Cleaners => cleaners;


    private void Start() {
        cleanerSelector = gameObject.GetComponent<CleanerSelector>();
        commandsDisplay = gameObject.GetComponent<CommandsDisplay>();

        startButton.gameObject.SetActive(true);
        restartButton.gameObject.SetActive(false);
        nextLevelButton.gameObject.SetActive(false);
        
        FreezeGame(true);
    }

    private void Update() {
        if (isInputEnabled) {
            if (Input.GetMouseButtonDown(0)) {
                if (commandsDisplay.CleanAreaIsActive) {
                    cleanerSelector.SelectedCleaner().GiveCleanTask(commandsDisplay.ReleaseCleanArea());
                }
                else if (commandsDisplay.DistractLineIsActive) {
                    if (commandsDisplay.DistractLineFirstClick) {
                        commandsDisplay.DistractLineFirstClick = false;
                    }
                    else {
                        cleanerSelector.SelectedCleaner().GiveDistractTask(commandsDisplay.ReleaseDistractLine());
                    }
                }
                else {
                    cleanerSelector.SelectedCleaner().MoveToClick();
                }
            }

            if (Input.GetMouseButtonDown(1)) {
                if (commandsDisplay.CleanAreaIsActive || commandsDisplay.DistractLineIsActive) {
                    commandsDisplay.HideCommandsVisualisation();
                }
                else {
                    cleanerSelector.SelectNext();
                }
            }

            if (Input.GetKeyDown(KeyCode.C)) {
                OnCleanButtonClick();
            }

            if (Input.GetKeyDown(KeyCode.H)) {
                OnHelpButtonClick();
            }

            if (Input.GetKeyDown(KeyCode.D)) {
                OnDistractButtonClick();
            }
        }
    }


    public void OnCleanButtonClick() {
        commandsDisplay.ShowCleanArea(true);
    }

    public void OnHelpButtonClick() {
        cleanerSelector.SelectedCleaner().GiveHelpTask();
    }

    public void OnDistractButtonClick() {
        commandsDisplay.ShowDistractLine(true);
    }


    public void OnStartButtonClick() {
        FreezeGame(false);
        startButton.gameObject.SetActive(false);
    }

    public void OnRestartButtonClick() {
        FreezeGame(false);
        nextLevelButton.gameObject.SetActive(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnNextLevelButtonClick() {
        FreezeGame(false);
        restartButton.gameObject.SetActive(false);
        SceneManager.LoadScene("LevelOne");
    }


    public void GameOver(bool win) {
        FreezeGame(true);
        if (win) {
            nextLevelButton.gameObject.SetActive(true);
        }
        else {
            restartButton.gameObject.SetActive(true);
        }
    }

    private void FreezeGame(bool freeze) {
        Time.timeScale = freeze ? 0 : 1;
        isInputEnabled = !freeze;
        if (Camera.main != null) Camera.main.eventMask = freeze ? LayerMask.GetMask("UI") : -1;
    }
}